<?php

// Travelforce singleton
require(dirname(__FILE__) . '/lib/Travelforce.php');

// Api requestor
require(dirname(__FILE__) . '/lib/ApiRequestor.php');

// Api resource
require(dirname(__FILE__) . '/lib/ApiResource.php');

// Resource objects
require(dirname(__FILE__) . '/lib/Air.php');
require(dirname(__FILE__) . '/lib/Bookings.php');

// Errors
require(dirname(__FILE__) . '/lib/Error/Base.php');
require(dirname(__FILE__) . '/lib/Error/ActionRequired.php');
require(dirname(__FILE__) . '/lib/Error/Api.php');
require(dirname(__FILE__) . '/lib/Error/Authentication.php');
require(dirname(__FILE__) . '/lib/Error/InvalidRequest.php');
require(dirname(__FILE__) . '/lib/Error/RequestFailed.php');

// Utilities
require(dirname(__FILE__) . '/lib/Util/Passenger.php');
require(dirname(__FILE__) . '/lib/Util/Util.php');
