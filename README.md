# Travelforce PHP bindings

You can sign up for a Travelforce account at https://travelforce.se.

## Requirements

PHP 5.3.3 and later.

## Composer

You can install the bindings via [Composer](http://getcomposer.org/). Add this to your `composer.json`:

```json
{
  "require": {
    "travelforce/travelforce-php": "1.*"
  }
}
```

Then install via:

```bash
composer install
```

To use the bindings, use Composer's [autoload](https://getcomposer.org/doc/00-intro.md#autoloading):

```php
require_once('vendor/autoload.php');
```

## Manual Installation

If you do not wish to use Composer, you can download the [latest release](https://github.com/travelforce/travelforce-php/releases). Then, to use the bindings, install the dependencies specified in `composer.json` and then include the `init.php` file.

```php
require_once('/path/to/travelforce-php/init.php');
```

## Getting Started

Simple usage looks like:

```php
\Travelforce\Travelforce::setApiKey('API_KEY');
$searchResults = \Travelforce\Air::search(array(
    'origin'             => 'STO',
    'destination'        => 'UME',
    'departure_date'     => '2016-06-19',
    'adults'             => 1,
    'children'           => 1
));

echo $searchResults;
```

## Documentation

Please see https://travelforce.se/docs/api for up-to-date documentation.

## Tests

In order to run tests first install [PHPUnit](http://packagist.org/packages/phpunit/phpunit) via [Composer](http://getcomposer.org/):

```bash
composer update --dev
```

To run the test suite:

```bash
./vendor/bin/phpunit
```