<?php

namespace Travelforce;

class Pnr extends ApiResource {
    public static function retrieve($pnr = null) {
        if (!$pnr) {
            $msg = 'You need to supply a PNR to be able to retrieve a booking.';

            throw new Error\InvalidRequest($msg);
        }

        $requestor = static::_getRequestor();
        $url       = static::baseUrl() . static::classUrl() . "/$pnr";

        $response = $requestor->request('GET', $url);

        return $response;
    }

    public static function className()
    {
        return 'bookings/pnr';
    }

    public static function void($pnr = null, $params = null) {
        if (!$pnr) {
            $msg = 'You need to supply a PNR to be able to void a booking\'s tickets.';

            throw new Error\InvalidRequest($msg);
        }

        $requestor = static::_getRequestor();
        $url       = static::baseUrl() . static::classUrl() . "/$pnr/void";

        $response = $requestor->request('POST', $url, $params);

        return $response;
    }

    public static function cancel($pnr = null, $params = null) {
        if (!$pnr) {
            $msg = 'You need to supply a PNR to be able to cancel a booking.';

            throw new Error\InvalidRequest($msg);
        }

        $requestor = static::_getRequestor();
        $url       = static::baseUrl() . static::classUrl() . "/$pnr/cancel";

        $response = $requestor->request('POST', $url, $params);

        return $response;
    }
}
