<?php

namespace Travelforce;

use GuzzleHttp\Client;

class ApiRequestor {
    private $_apiKey;
    private $_apiBase;
    private static $_httpClient;

    public function __construct($apiKey = null, $apiBase = null) {
        $this->_apiKey = $apiKey;

        if (!$apiBase) {
            $apiBase = Travelforce::$apiBase;
        }

        $this->_apiBase = $apiBase;
    }

    public function request($method, $url, $params = null, $headers = []) {
        list($rBody, $rCode, $rHeaders, $apiKey) = $this->_requestRaw(
            $method, $url, $params, $headers
        );

        // Try to interpret the API response
        $resp = $this->_interpretResponse($rBody, $rCode, $rHeaders);

        return $resp;
    }

    private function _requestRaw($method, $url, $params, $headers) {
        $myApiKey = $this->_apiKey;

        if (!$myApiKey) {
            $myApiKey = Travelforce::$apiKey;
        }

        if (!$myApiKey) {
            $msg = 'No API key provided.  (HINT: set your API key using ' .
                '"Travelforce::setApiKey(<API-KEY>)".  You can generate API keys from ' .
                'the Travelforce web interface.  See https://travelforce.se/api for ' .
                'details, or email support@travelforce.se if you have any questions.';

            throw new Error\Authentication($msg);
        }

        $absUrl = $this->_apiBase . $url;
        // $params = self::_encodeObjects($params);
        $langVersion = phpversion();
        $uname = php_uname();
        $ua = array(
            'bindings_version' => Travelforce::VERSION,
            'lang' => 'php',
            'lang_version' => $langVersion,
            'publisher' => 'travelforce',
            'uname' => $uname,
        );
        $defaultHeaders = array(
            'X-Travelforce-Client-User-Agent' => json_encode($ua),
            'User-Agent' => 'Travelforce/v1 PhpBindings/' . Travelforce::VERSION,
            'Authorization' => 'Basic ' . base64_encode($myApiKey . ':'),
        );

        if (Travelforce::$apiVersion) {
            $defaultHeaders['Travelforce-Version'] = Travelforce::$apiVersion;
        }

        $defaultHeaders['Content-Type'] = 'application/x-www-form-urlencoded';

        $combinedHeaders = array_merge($defaultHeaders, $headers);
        $rawHeaders = [];

        foreach ($combinedHeaders as $header => $value) {
            $rawHeaders[] = $header . ': ' . $value;
        }

        if (isset($params) && $method === 'POST') {
            $params = [
                'form_params' => $params
            ];
        }

        if (isset($params)) {
            $resp = $this->httpClient()->request($method, $url, $params);
        } else {
            $resp = $this->httpClient()->request($method, $url);
        }

        return [$resp->getBody(), $resp->getStatusCode(), $resp->getHeaders(), $myApiKey];
    }

    /**
     * @param string $rbody A JSON string.
     * @param int $rcode
     * @param array $rheaders
     * @param array $resp
     *
     * @throws Error\InvalidRequest if the error is caused by the user.
     * @throws Error\Authentication if the error is caused by a lack of
     *    permissions.
     * @throws Error\Card if the error is the error code is 402 (payment
     *    required)
     * @throws Error\RateLimit if the error is caused by too many requests
     *    hitting the API.
     * @throws Error\Api otherwise.
     */
    public function handleApiError($rbody, $rcode, $rheaders, $resp) {
        if (!is_object($resp) || !isset($resp->error)) {
            $msg = "Invalid response object from API: $rbody " .
                "(HTTP response code was $rcode)";

            throw new Error\Api($msg, $rcode, $rbody, $resp, $rheaders);
        }

        $error = $resp->error;
        $msg   = isset($error->message) ? $error->message : null;
        $code  = isset($error->code) ? $error->code : null;
        $type  = isset($error->type) ? $error->type : null;
        $data  = isset($error->data) ? $error->data : null;

        switch ($rcode) {
            case 400:
                throw new Error\InvalidRequest($msg, $type, $code, $rbody, $resp, $rheaders, $data);

                // intentional fall-through
            case 404:
                throw new Error\InvalidRequest($msg, $type, $code, $rbody, $resp, $rheaders, $data);
            case 401:
                throw new Error\Authentication($msg, $type, $code, $rbody, $resp, $rheaders, $data);
            case 402:
                if ($type === 'request_failed') {
                    throw new Error\RequestFailed($msg, $type, $code, $rcode, $rbody, $resp, $rheaders, $data);
                } else if ($type === 'action_required') {
                    throw new Error\ActionRequired($msg, $type, $code, $rcode, $rbody, $resp, $rheaders, $data);
                }
            case 429:
                throw new Error\RateLimit($msg, $type, $rcode, $rbody, $resp, $rheaders, $data);
            default:
                throw new Error\Api($msg, $type, $code, $rbody, $resp, $rheaders, $data);
        }
    }

    private function _interpretResponse($rBody, $rCode, $rHeaders) {
        try {
            $resp = json_decode($rBody);
        } catch (Exception $e) {
            $msg = "Invalid response body from API: $rBody " .
                "(HTTP response code was $rCode)";

            throw new Error\Api($msg, $rCode, $rBody);
        }

        if ($rCode < 200 || $rCode >= 300) {
            $this->handleApiError($rBody, $rCode, $rHeaders, $resp);
        }

        return $resp;
    }
    /**
     * Allow for the HTTP client to be overridden for test purposes.
     * @param mixed $client An instance of a compatible HTTP client, such as Guzzle.
     */
    public static function setHttpClient($client)
    {
        self::$_httpClient = $client;
    }

    public function httpClient() {
        if (!self::$_httpClient) {
            self::$_httpClient = new Client([
                'base_uri' => $this->_apiBase,
                'auth' => [$this->_apiKey, ''],
                'http_errors' => false
            ]);
        }

        return self::$_httpClient;
    }
}