<?php

namespace Travelforce;

class ApiResource {
    public static function baseUrl() {
        return Travelforce::$apiBase;
    }

    /**
     * @return string The name of the class, with namespacing and underscores
     *    stripped.
     */
    public static function className() {
        $class = get_called_class();

        // Useful for namespaces: Foo\Charge
        if ($postfixNamespaces = strrchr($class, '\\')) {
            $class = substr($postfixNamespaces, 1);
        }

        // Useful for underscored 'namespaces': Foo_Charge
        if ($postfixFakeNamespaces = strrchr($class, '')) {
            $class = $postfixFakeNamespaces;
        }

        if (substr($class, 0, strlen('Travelforce')) == 'Travelforce') {
            $class = substr($class, strlen('Travelforce'));
        }

        $class = str_replace('_', '', $class);
        $name = urlencode($class);
        $name = strtolower($name);

        return $name;
    }

    /**
     * @return string The endpoint URL for the given class.
     */
    public static function classUrl() {
        $base = static::className();
        return "/v1/${base}";
    }

    /**
     * @return string The full API URL for this API resource.
     */
    public function instanceUrl() {
        $id = static::$id;

        if ($id === null) {
            $class = get_called_class();
            $message = "Could not determine which URL to request: " .
                "$class instance has invalid ID: $id";

            throw new Error\InvalidRequest($message, null);
        }

        $base = static::classUrl();
        $extn = urlencode($id);

        return "$base/$extn";
    }

    protected static $_requestor = null;

    protected static function _getRequestor() {
        if (is_null(static::$_requestor))
            static::$_requestor = new ApiRequestor(Travelforce::getApiKey(), static::baseUrl());

        return static::$_requestor;
    }
}