<?php

namespace Travelforce;

class Travelforce {
    // @var string The Travelforce API key to be used for requests.
    public static $apiKey;

    // @var string|null The version of the Travelforce API to use for requests.
    public static $apiVersion = null;

    // @var string The base URL for the Travelforce API.
    public static $apiBase = 'https://api.travelforce.se';

    const VERSION = '1.0.0';

    /**
     * @return string The API key used for requests.
     */
    public static function getApiKey() {
        return self::$apiKey;
    }

    /**
     * Sets the API key to be used for requests.
     *
     * @param string $apiKey
     */
    public static function setApiKey($apiKey) {
        self::$apiKey = $apiKey;
    }

    /**
     * Sets the API base URL to be used for requests.
     *
     * @param string $apiBase
     */
    public static function setApiBase($apiBase) {
        self::$apiBase = $apiBase;
    }

    /**
     * @return string The API base URL used for requests.
     */
    public static function getApiBase() {
        return self::$apiBase;
    }
}