<?php

namespace Travelforce;

class Air extends ApiResource {
    public static function search($params) {
        $requestor = static::_getRequestor();
        $url       = static::baseUrl() . static::classUrl() . '/search';

        $response = $requestor->request(
            'POST',
            $url,
            $params
        );

        return $response;
    }

    /**
     * Usage:
     *
     * $responses = Air::multiSearch([
     *     'economy' => [
     *         // Params
     *     ],
     *     'business' => [
     *         // Params
     *     ]
     * ]);
     */
    public static function multiSearch($requests) {
        $requestor = static::_getRequestor();
        $url       = static::baseUrl() . static::classUrl() . '/search';

        $promises = [];

        foreach ($requests as $name => $params) {
            $promises[$name] = $requestor->httpClient()->requestAsync(
                'POST',
                $url, [
                    'form_params' => $params
                ]
            );
        }

        $responseBodies = [];

        // Unwrapped promises
        $responses = \GuzzleHttp\Promise\unwrap($promises);

        foreach ($responses as $name => $response) {
            if ($response->getStatusCode() !== 200) {
                $resp = [];
            } else {
                $resp = json_decode($response->getBody());
            }

            $responseBodies[$name] = $resp;
        }

        return $responseBodies;
    }

    public static function book($params) {
        $requestor = static::_getRequestor();
        $url       = static::baseUrl() . static::classUrl() . '/book';

        // Make sure that the itinerary is in the correct format
        if (isset($params['itinerary'])) {
            $itinerary = $params['itinerary'];

            $params['itinerary'] = json_encode($itinerary);

            if (json_last_error() != JSON_ERROR_NONE) {
                $message = 'Could not parse itinerary parameter.' .
                    'The parameter is expected to be a JSON string, an object or an array.';

                throw new Error\InvalidRequest($message);
            }
        }

        $response = $requestor->request(
            'POST',
            $url,
            $params
        );

        return $response;
    }
}