<?php

namespace Travelforce;

class Bookings extends ApiResource {
    public static function retrieve($id = null) {
        if (!$id) {
            $msg = 'You need to supply a booking ID to be able to retrieve a booking.';

            throw new Error\InvalidRequest($msg);
        }

        $requestor = static::_getRequestor();
        $url       = static::baseUrl() . static::classUrl() . "/$id";

        $response = $requestor->request(
            'GET',
            $url
        );

        return $response;
    }

    public static function void($id = null, $params = null) {
        if (!$id) {
            $msg = 'You need to supply a booking ID to be able to void a booking\'s tickets.';

            throw new Error\InvalidRequest($msg);
        }

        $requestor = static::_getRequestor();
        $url       = static::baseUrl() . static::classUrl() . "/$id/void";

        $response = $requestor->request(
            'POST',
            $url,
            $params
        );

        return $response;
    }

    public static function ticket($id = null, $params = null) {
        if (!$id) {
            $msg = 'You need to supply a booking ID to be able to ticket a booking.';

            throw new Error\InvalidRequest($msg);
        }

        $requestor = static::_getRequestor();
        $url       = static::baseUrl() . static::classUrl() . "/$id/ticket";

        $response = $requestor->request(
            'POST',
            $url,
            $params
        );

        return $response;
    }

    public static function queue($id = null, $params = null) {
        if (!$id) {
            $msg = 'You need to supply a booking ID to be able to queue a booking.';

            throw new Error\InvalidRequest($msg);
        }

        $requestor = static::_getRequestor();
        $url       = static::baseUrl() . static::classUrl() . "/$id/queue";

        $response = $requestor->request(
            'POST',
            $url,
            $params
        );

        return $response;
    }

    public static function cancel($id = null, $params = null) {
        if (!$id) {
            $msg = 'You need to supply a booking ID to be able to cancel a booking.';

            throw new Error\InvalidRequest($msg);
        }

        $requestor = static::_getRequestor();
        $url       = static::baseUrl() . static::classUrl() . "/$id/cancel";

        $response = $requestor->request(
            'POST',
            $url,
            $params
        );

        return $response;
    }

    public static function modify($id = null, $params = null) {
        if (!$id) {
            $msg = 'You need to supply a booking ID to be able to modify a booking.';

            throw new Error\InvalidRequest($msg);
        }

        $requestor = static::_getRequestor();
        $url       = static::baseUrl() . static::classUrl() . "/$id/modify";

        $response = $requestor->request(
            'POST',
            $url,
            $params
        );

        return $response;
    }
}
