<?php

namespace Travelforce\Util;

class Passenger {
    private $_passenger = [];

    public function __construct($firstName, $lastName, $type, $title) {
        $this->_passenger['first_name'] = $firstName;
        $this->_passenger['last_name']  = $lastName;
        $this->_passenger['type']       = $type;
        $this->_passenger['title']      = $title;

        return $this;
    }

    public function setDateOfBirth($dob) {
        $this->_passenger['date_of_birth'] = $dob;

        return $this;
    }

    public function addLoyaltyCard($carrier, $number) {
        if (!is_array($this->_passenger['loyalty_cards']))
            $this->_passenger['loyalty_cards'] = [];

        $this->_passenger['loyalty_cards'][] = [
            'supplier_code' => $carrier,
            'card_number' => $number
        ];

        return $this;
    }

    public function getPassenger() {
        return $this->_passenger;
    }
}