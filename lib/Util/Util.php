<?php

namespace Travelforce\Util;

class Util {
    public static function createPassenger($firstName, $lastName, $type, $title,
                                           $loyaltyCards = null, $dateOfBirth = null) {
        $passenger = new Passenger($firstName, $lastName, $type, $title);

        if (isset($loyaltyCards) && is_array($loyaltyCards)) {
            foreach ($loyaltyCards as $loyaltyCard)
                $passenger->addLoyaltyCard($loyaltyCard['carrier'], $loyaltyCard['number']);
        }

        if (isset($dateOfBirth))
            $passenger->setDateOfBirth($dateOfBirth);

        return $passenger->getPassenger();
    }
}